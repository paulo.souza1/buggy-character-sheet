# RPG Character Sheet

A simple character sheet app, to track your character's stats and items.

The codebase has some bugs and is intended to be used as an example of how we can use debugging techniques and heuristics for an effective bug fixing flow.

## How to run

First you need to install the dependencies:

```shell
$ npm install
```

Then, you can run the development server:

```shell
$ npm run start
```
