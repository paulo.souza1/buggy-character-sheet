import React from "react";

import "./index.css";

export const CharacterBio = ({ className, name, health, mana, onNameChange, onHealthChange, onManaChange }) => {
  const removeNonNumberCharacters = (value) => value?.replace(/[^0-9]/g, "") ?? "";

  const containerClass = `character-bio ${className}`.trim();

  return (
    <section className={containerClass}>
      <label className="name-label">
        Character name
        <input
          value={name}
          name="name"
          className="name-input"
          placeholder="Your character's name"
          onInput={(e) => onNameChange(e.target.value)}
        />
      </label>

      <div className="bio-points-container">
        <label className="bio-points health-points">
          <input
            value={health}
            name="health"
            onInput={(e) => onHealthChange(removeNonNumberCharacters(e.target.value))}
          />
          Health points
        </label>

        <label className="bio-points mana-points">
          <input name="mana" value={mana} onInput={(e) => onManaChange(removeNonNumberCharacters(e.target.value))} />
          Mana points
        </label>
      </div>
    </section>
  );
};
