import React from "react";

import { maxInventoryItems } from "../../../constants/character";

import "./index.css";

const replaceInventoryItemInList = (itemIndex, newItem, list) =>
  list
    .slice(0, itemIndex)
    .concat(newItem)
    .concat(list.slice(itemIndex + 1));

export const CharacterInventory = ({ className, items, onItemsChange }) => {
  const itemsEntries =
    items.length >= maxInventoryItems ? items : items.concat(Array(maxInventoryItems - items.length).fill(""));

  const handleItemChange = (index, e) => {
    const newItems = replaceInventoryItemInList(index, e.target.value, items);
    onItemsChange(newItems);
  };

  return (
    <section className={className}>
      <h3 className="inventory-title">Inventory</h3>

      {itemsEntries.map((item, index) => (
        <input
          value={item}
          key={`inventory-item-${index}`}
          name={`inventory-item-${index}`}
          className="inventory-item-name"
          placeholder={`Item #${index + 1}`}
          onInput={(e) => handleItemChange(index, e)}
        />
      ))}
    </section>
  );
};
