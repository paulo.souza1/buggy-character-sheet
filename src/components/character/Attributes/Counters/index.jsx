import React from "react";

import { maxPointsPerAttribute } from "../../../../constants/character";
import { attributesColorMap } from "../colorMap";

import "./index.css";

export const CharacterAttributesCounters = ({ attributes, onAttributeChange }) => (
  <div className="attributes-counters">
    {Object.entries(attributes).map(([name, value]) => (
      <div key={`${name}-counter`} className="attribute-counter">
        <span className="attribute-color" style={{ backgroundColor: attributesColorMap[name] }}></span>
        <label className="attribute-name">
          {name}

          <input
            name={name}
            type="number"
            value={value}
            max={maxPointsPerAttribute}
            min={0}
            className="attribute-value"
            onInput={(event) => onAttributeChange(name, event)}
          />
        </label>
      </div>
    ))}
  </div>
);
