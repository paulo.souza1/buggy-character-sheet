import React, { useState } from "react";

import { CharacterAttributesCounters } from "./Counters";
import { CharacterAttributesLevels } from "./Levels";
import { CharacterAttributesPoints } from "./Points";
import { maxPointsPerAttribute } from "../../../constants/character";

import "./index.css";

export const CharacterAttributes = ({ className, attributes, onAttributesChange }) => {
  const [spentPoints, setSpentPoints] = useState(0);

  const handleAttributeChange = (name, event) => {
    const newAttributes = { ...attributes, [name]: Number(event.target.value) };

    onAttributesChange(newAttributes);

    const totalPointsSpent = Object.values(newAttributes).reduce((total, points) => total + points, 0);

    setSpentPoints(totalPointsSpent);
  };

  const remainingPoints = Object.keys(attributes).length * maxPointsPerAttribute - spentPoints;

  const containerClass = `attributes ${className}`.trim();

  return (
    <section className={containerClass}>
      <CharacterAttributesCounters attributes={attributes} onAttributeChange={handleAttributeChange} />

      <CharacterAttributesPoints spentPoints={spentPoints} remainingPoints={remainingPoints} />

      <CharacterAttributesLevels attributes={attributes} />
    </section>
  );
};
