export const attributesColorMap = {
  strength: "#FD7F6F",
  intelligence: "#7EB0D5",
  endurance: "#B2E061",
  dexterity: "#BD7EBE",
  reflexes: "#FFB55A",
  charisma: "#FFEE65",
};
