import React from "react";

import "./index.css";

export const CharacterAttributesPoints = ({ spentPoints, remainingPoints }) => (
  <div className="attribute-points">
    <div className="points">
      <p className="points-value" data-testid="spent-points">
        {spentPoints}
      </p>

      <p className="points-label">Spent points</p>
    </div>

    <div className="points">
      <p className="points-value" data-testid="remaining-points">
        {remainingPoints}
      </p>

      <p className="points-label">Remaining points</p>
    </div>
  </div>
);
