import React from "react";

import { attributesColorMap } from "../colorMap";

import "./index.css";

const getHighestAttribute = (attributes) => Object.values(attributes).sort((a, b) => b - a)[0];

const buildNormalizedAttributesList = (attributes) => {
  const highestAttribute = getHighestAttribute(attributes);

  const attributesList = Object.entries(attributes);

  return attributesList.map(([name, value]) => [name, value / highestAttribute]);
};

export const CharacterAttributesLevels = ({ attributes }) => {
  const normalizedAttributes = buildNormalizedAttributesList(attributes);

  const sortedNormalizedAttributes = normalizedAttributes.sort(([, valueA], [, valueB]) => valueB - valueA);

  return (
    <div className="attribute-levels">
      {sortedNormalizedAttributes.map(([name, value]) => (
        <div key={`${name}-level`} className="level-container">
          <div
            className="level-fill-gauge"
            data-testid={`level-fill-gauge-${name}`}
            style={{
              backgroundColor: attributesColorMap[name],
              height: `${value * 100}%`,
            }}
          />
        </div>
      ))}
    </div>
  );
};
