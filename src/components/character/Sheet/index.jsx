/* eslint-disable no-restricted-globals */
import React, { useState } from "react";

import { saveCharacterData } from "../../../services/saveCharacterData";
import { clearSavedCharacterData } from "../../../services/clearSavedCharacterData";
import { defaultCharacterData } from "../../../constants/character";

import { CharacterAttributes } from "../Attributes";
import { CharacterInventory } from "../Inventory";
import { CharacterBio } from "../Bio";

import "./index.css";

export const CharacterSheet = ({ initialCharacterData = defaultCharacterData }) => {
  const [characterData, setCharacterData] = useState(initialCharacterData);

  const handleSaveCharacter = () => {
    saveCharacterData(characterData);

    alert("Saved character successfully!");
  };

  const handleResetCharacter = () => {
    const shouldReset = confirm("Do you really want to reset your character?");

    if (!shouldReset) return;

    setCharacterData(defaultCharacterData);

    clearSavedCharacterData();
  };

  return (
    <article className="sheet">
      <header className="sheet-section sheet-header">
        <img className="sheet-title-leading-icon" src="/shield-and-sword.png" height={50} alt="" />

        <h1 className="sheet-title-text">Character Sheet</h1>

        <img className="sheet-title-trailing-icon" src="/shield-and-sword.png" height={50} alt="" />
      </header>

      <CharacterBio
        className="sheet-section"
        name={characterData.name}
        onNameChange={(name) => setCharacterData((data) => ({ ...data, name }))}
        health={characterData.health}
        onHealthChange={(health) => setCharacterData((data) => ({ ...data, health }))}
        mana={characterData.mana}
        onManaChange={(mana) => setCharacterData((data) => ({ ...data, mana }))}
      />

      <CharacterAttributes
        className="sheet-section"
        attributes={characterData.attributes}
        onAttributesChange={(attributes) => setCharacterData((data) => ({ ...data, attributes }))}
      />

      <CharacterInventory
        className="sheet-section"
        items={characterData.items}
        onItemsChange={(items) => setCharacterData((data) => ({ ...data, items }))}
      />

      <div className="sheet-actions-container">
        <button type="submit" className="sheet-action-button save-button" onClick={handleSaveCharacter}>
          Save
        </button>

        <button type="button" className="sheet-action-button reset-button" onClick={handleResetCharacter}>
          Reset
        </button>
      </div>
    </article>
  );
};
