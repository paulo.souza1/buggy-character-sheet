import React from "react";
import ReactDOM from "react-dom/client";

import { CharacterSheet } from "./components/character/Sheet";
import { retrieveSavedCharacterData } from "./services/retrieveSavedCharacterData";

import "./index.css";

let savedCharacterData = retrieveSavedCharacterData();

const rootContainer = document.getElementById("root");

const root = ReactDOM.createRoot(rootContainer);

root.render(<CharacterSheet initialCharacterData={savedCharacterData} />);
