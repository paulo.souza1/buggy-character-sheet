import { characterDataStorageKey } from "../constants/character";

export const retrieveSavedCharacterData = () => {
  const savedCharacterData = localStorage.getItem(characterDataStorageKey);

  if (!savedCharacterData) return undefined;

  try {
    return JSON.parse(savedCharacterData);
  } catch {
    return undefined;
  }
};
