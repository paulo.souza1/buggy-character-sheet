import { characterDataStorageKey } from "../constants/character";

export const saveCharacterData = (data) => localStorage.setItem(characterDataStorageKey, JSON.stringify(data));
