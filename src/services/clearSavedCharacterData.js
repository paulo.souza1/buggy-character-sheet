import { characterDataStorageKey } from "../constants/character";

export const clearSavedCharacterData = () => localStorage.removeItem(characterDataStorageKey);
