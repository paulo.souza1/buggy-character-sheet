import { characterDataStorageKey } from "../../constants/character";
import { clearSavedCharacterData } from "../../services/clearSavedCharacterData";

describe("Services / ClearSavedCharacterData", () => {
  it("should remove any stored character data", () => {
    localStorage.setItem(characterDataStorageKey, '{"name": "Foo"}');

    clearSavedCharacterData();

    expect(localStorage.getItem(characterDataStorageKey)).toBeNull();
  });
});
