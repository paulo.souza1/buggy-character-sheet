import { characterDataStorageKey } from "../../constants/character";
import { retrieveSavedCharacterData } from "../../services/retrieveSavedCharacterData";

describe("Services / RetrieveSavedCharacterData", () => {
  describe("when there is no saved passenger data stored", () => {
    it("should return undefined", () => {
      localStorage.clear();

      const savedData = retrieveSavedCharacterData();

      expect(savedData).toBeUndefined();
    });
  });

  describe("when there is saved passenger data stored", () => {
    describe("and the data can't be parsed", () => {
      it("should return undefined", () => {
        localStorage.setItem(characterDataStorageKey, "garbageData");

        const savedData = retrieveSavedCharacterData();

        expect(savedData).toBeUndefined();
      });
    });

    describe("and the data can be parsed succesfully", () => {
      it("should return the parsed data", () => {
        localStorage.setItem(characterDataStorageKey, '{"name": "Foo", "health": 150, "mana": 50}');

        const savedData = retrieveSavedCharacterData();

        expect(savedData).toEqual({ name: "Foo", health: 150, mana: 50 });
      });
    });
  });
});
