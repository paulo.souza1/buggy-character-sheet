import { characterDataStorageKey } from "../../constants/character";
import { saveCharacterData } from "../../services/saveCharacterData";

describe("Services / SaveCharacterData", () => {
  it("should store the stringfied character data", () => {
    saveCharacterData({ name: "Foo, The Great", health: 300, items: ["Sword"] });

    const savedData = localStorage.getItem(characterDataStorageKey);

    expect(savedData).toEqual('{"name":"Foo, The Great","health":300,"items":["Sword"]}');
  });
});
