import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render, screen } from "@testing-library/react";

import { CharacterSheet } from "../../components/character/Sheet";
import { defaultCharacterData, maxInventoryItems } from "../../constants/character";

const savedCharacterDataMock = defaultCharacterData;

describe("Components / Sheet", () => {
  describe("when no custom initial data is provided", () => {
    it("should start with an empty character name", () => {
      render(<CharacterSheet />);

      const nameField = screen.getByLabelText("Character name");

      expect(nameField).toHaveValue("");
    });

    it("should start with the default amount of health points", () => {
      render(<CharacterSheet />);

      const healthField = screen.getByLabelText("Health points");

      expect(healthField).toHaveValue("100");
    });

    it("should start with the default amount of mana points", () => {
      render(<CharacterSheet />);

      const manaField = screen.getByLabelText("Mana points");

      expect(manaField).toHaveValue("50");
    });

    it("should start with zeroed attributes", () => {
      render(<CharacterSheet />);

      expect(screen.getByLabelText("strength")).toHaveValue(0);
      expect(screen.getByLabelText("intelligence")).toHaveValue(0);
      expect(screen.getByLabelText("endurance")).toHaveValue(0);
      expect(screen.getByLabelText("dexterity")).toHaveValue(0);
      expect(screen.getByLabelText("reflexes")).toHaveValue(0);
      expect(screen.getByLabelText("charisma")).toHaveValue(0);
    });

    it("should start with zero spent points", () => {
      render(<CharacterSheet />);

      expect(screen.getByTestId("spent-points")).toHaveTextContent("0");
    });

    it("should start with the maximum remaining points", () => {
      render(<CharacterSheet />);

      expect(screen.getByTestId("remaining-points")).toHaveTextContent("72");
    });

    it("should start with no items in the inventory", () => {
      render(<CharacterSheet />);

      for (let i = 1; i <= maxInventoryItems; i++) {
        expect(screen.getByPlaceholderText(`Item #${i}`)).toHaveValue("");
      }
    });
  });

  describe("when custom initial data is provided", () => {
    it("should start with the provided initial name", () => {
      render(<CharacterSheet initialCharacterData={{ ...savedCharacterDataMock, name: "Foo Bar, The Great" }} />);

      const nameField = screen.getByLabelText("Character name");

      expect(nameField).toHaveValue("Foo Bar, The Great");
    });

    it("should start with the provided amount of health points", () => {
      render(<CharacterSheet initialCharacterData={{ ...savedCharacterDataMock, health: 75 }} />);

      const healthField = screen.getByLabelText("Health points");

      expect(healthField).toHaveValue("75");
    });

    it("should start with the provided amount of mana points", () => {
      render(<CharacterSheet initialCharacterData={{ ...savedCharacterDataMock, mana: 33 }} />);

      const manaField = screen.getByLabelText("Mana points");

      expect(manaField).toHaveValue("33");
    });

    it("should start with the provided attribute distribution", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 0,
              charisma: 4,
            },
          }}
        />
      );

      expect(screen.getByLabelText("strength")).toHaveValue(3);
      expect(screen.getByLabelText("intelligence")).toHaveValue(5);
      expect(screen.getByLabelText("endurance")).toHaveValue(1);
      expect(screen.getByLabelText("dexterity")).toHaveValue(2);
      expect(screen.getByLabelText("reflexes")).toHaveValue(0);
      expect(screen.getByLabelText("charisma")).toHaveValue(4);
    });

    it("should start with the correct levels in the attribute gauges", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      expect(screen.getByTestId("level-fill-gauge-strength")).toHaveStyle("height: 60%");
      expect(screen.getByTestId("level-fill-gauge-intelligence")).toHaveStyle("height: 100%");
      expect(screen.getByTestId("level-fill-gauge-endurance")).toHaveStyle("height: 20%");
      expect(screen.getByTestId("level-fill-gauge-dexterity")).toHaveStyle("height: 40%");
      expect(screen.getByTestId("level-fill-gauge-reflexes")).toHaveStyle("height: 20%");
      expect(screen.getByTestId("level-fill-gauge-charisma")).toHaveStyle("height: 80%");
    });

    it("should start with the provided inventory items", () => {
      render(
        <CharacterSheet
          initialCharacterData={{ ...savedCharacterDataMock, items: ["Greatsword", "Bag of Holding", "Diamond dust"] }}
        />
      );

      expect(screen.getByPlaceholderText("Item #1")).toHaveValue("Greatsword");
      expect(screen.getByPlaceholderText("Item #2")).toHaveValue("Bag of Holding");
      expect(screen.getByPlaceholderText("Item #3")).toHaveValue("Diamond dust");
    });
  });

  describe("when the user increases an attribute", () => {
    it("should increase the attribute level gauge accordingly", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      fireEvent.input(screen.getByLabelText("endurance"), { target: { value: 2 } });

      expect(screen.getByTestId("level-fill-gauge-endurance")).toHaveStyle("height: 40%");
    });

    it("should increase the spent points amount accordingly", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      fireEvent.input(screen.getByLabelText("endurance"), { target: { value: 2 } });

      expect(screen.getByTestId("spent-points")).toHaveTextContent("17");
    });

    it("should decrease the remaining points amount accordingly", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      fireEvent.input(screen.getByLabelText("endurance"), { target: { value: 2 } });

      expect(screen.getByTestId("remaining-points")).toHaveTextContent("55");
    });
  });

  describe("when the user decreases an attribute", () => {
    it("should decrease the attribute level gauge accordingly", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      fireEvent.input(screen.getByLabelText("charisma"), { target: { value: 3 } });

      expect(screen.getByTestId("level-fill-gauge-charisma")).toHaveStyle("height: 60%");
    });

    it("should decrease the spent points amount accordingly", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      fireEvent.input(screen.getByLabelText("endurance"), { target: { value: 0 } });

      expect(screen.getByTestId("spent-points")).toHaveTextContent("15");
    });

    it("should increase the remaining points amount accordingly", () => {
      render(
        <CharacterSheet
          initialCharacterData={{
            ...savedCharacterDataMock,
            attributes: {
              strength: 3,
              intelligence: 5,
              endurance: 1,
              dexterity: 2,
              reflexes: 1,
              charisma: 4,
            },
          }}
        />
      );

      fireEvent.input(screen.getByLabelText("endurance"), { target: { value: 0 } });

      expect(screen.getByTestId("remaining-points")).toHaveTextContent("57");
    });
  });
});
