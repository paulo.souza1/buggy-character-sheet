export const defaultCharacterData = {
  name: "",
  health: 100,
  mana: 50,
  attributes: {
    strength: 0,
    intelligence: 0,
    endurance: 0,
    dexterity: 0,
    reflexes: 0,
    charisma: 0,
  },
  items: [],
};

export const maxPointsPerAttribute = 12;

export const maxInventoryItems = 6;

export const characterDataStorageKey = "CharacterData";
